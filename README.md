#  Below is a classic problem, often given as a technical test for developers. #

## To run this code ##


```
#!bash

git clone git@bitbucket.org:TomShacham/drawing_in_haskell.git

ghc --make drawing

./drawing
```

Start drawing! (>\*_\*)>


```
#!bash

C 10 10 #makes a 10x10 canvas

R 2 2 9 9 #draws a rectangle from (2,2) to (9,9)

B 5 5 #fills in this rectangle

U #undo 

L 0 0 0 9 #draw a line down the left hand side from (0,0) to (0,9)
```

Left to do: 

* Stack commands so that re-do can be implemented nicely
* Use a Vector to represent the canvas, and convert to String only for IO


## Description

You're given the task of writing a simple console version of a drawing program.
At this time, the functionality of the program is quite limited but this might
change in the future. In a nutshell, the program should work as follows:

1. create a new canvas
2. start drawing on the canvas by issuing various commands
3. quit

At the moment, the program should support the following commands:

 Command         | Description                                         
-----------------|-----------------------------------------------------
 `C w h`	        | Should create a new canvas of width w and height h.                  
 `L x1 y1 x2 y2`	| Should create a new line from (x1,y1) to (x2,y2). Currently only horizontal or vertical lines are supported. Horizontal and vertical lines will be drawn using the 'x' character.
 `R x1 y1 x2 y2`	| Should create a new rectangle, whose upper left corner is (x1,y1) and lower right corner is (x2,y2). Horizontal and vertical lines will be drawn using the 'x' character.             
 `B x y c`	      | Should fill the entire area connected to (x,y) with "colour" c. The behaviour of this is the same as that of the "bucket fill" tool in paint programs.                                           
 `Q`             | Should quit the program.                                          

## Sample I/O

Below is a sample run of the program. User input is prefixed with `enter command:`.

    enter command: C 20 4
    ----------------------
    |                    |
    |                    |
    |                    |
    |                    |
    ----------------------
    
    enter command: L 1 2 6 2
    ----------------------
    |                    |
    |xxxxxx              |
    |                    |
    |                    |
    ----------------------
    
    enter command: L 6 3 6 4
    ----------------------
    |                    |
    |xxxxxx              |
    |     x              |
    |     x              |
    ----------------------
    
    enter command: R 16 1 20 3
    ----------------------
    |               xxxxx|
    |xxxxxx         x   x|
    |     x         xxxxx|
    |     x              |
    ----------------------
    
    enter command: B 10 3 o
    ----------------------
    |oooooooooooooooxxxxx|
    |xxxxxxooooooooox   x|
    |     xoooooooooxxxxx|
    |     xoooooooooooooo|
    ----------------------
    
    enter command: Q