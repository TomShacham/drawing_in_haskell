module Zipper where

	data Tree a = Node a (Tree a) (Tree a) | Empty
	data Crumb a = LeftCrumb a (Tree a) | RightCrumb a (Tree a)
	data LZipr a = LZipr ([a],[a]) deriving Show
	data TZipr a = TZipr (Tree a, Tree a)

	class Zipper z where
		getL :: z a -> [a]
		getR :: z a -> [a]
		moveLeft :: z a -> z a
		moveRight :: z a -> z a
		lmap :: (a -> a) -> z a -> z a
		rmap :: (a -> a) -> z a -> z a
		zmap :: (a -> a) -> z a -> z a

	instance Zipper LZipr where
		getL (LZipr (x,y)) = x
		getR (LZipr (x,y)) = y
		moveLeft (LZipr ((xs), (y:ys))) = LZipr ((y:xs), ys)
		moveRight (LZipr ((x:xs), (ys))) = LZipr ((xs), (x:ys))
		lmap f (LZipr (xs, ys)) = LZipr (map f xs, ys)
		rmap f (LZipr (xs, ys)) = LZipr (xs, map f ys)
		zmap f z = rmap f (lmap f z) 

	instance Zipper TZipr where
		getL (TZipr (x,y)) = x
		getR (TZipr (x,y)) = y
		moveLeft (TZipr (Node x l0 r0, Node y l1 r1)) = 