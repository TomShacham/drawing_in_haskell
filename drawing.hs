import Control.Monad.Writer

type Canvas = [String]
type Portfolio = [Canvas]
type CanvasStack = Writer Portfolio Canvas
type Coord = (Int, Int)

main = repl $ writer (["start"],[["start"]])

repl :: CanvasStack -> IO String
repl stack = do cmd <- getLine
                let coords = toCoords cmd
                if null cmd then repl stack
                else case head cmd of 'C' -> printCanvasThenRepl $ push (makeNewCanvas coords) $ onto stack
                                      'L' -> printCanvasThenRepl $ push (drawLineOnCanvas canvas coords) $ onto stack
                                      'R' -> printCanvasThenRepl $ push (drawRectangle (rectCoordsToLineCoords coords) canvas) $ onto stack
                                      'B' -> printCanvasThenRepl $ push (bucketFill canvas coords) $ onto stack
                                      'U' -> printCanvasThenRepl $ undo stack
                                      _ -> do putStrLn "Unknown command\n"
                                              repl stack

                  where onto = id
                        canvas = head $ execWriter stack
                        push canvas stack = do mapWriter (\(prevCanvas,stk) -> (canvas, canvas : stk)) stack 
                                               return canvas
                        toCoords cmd = case map read . words $ tail cmd of [x1, y1, x2, y2] -> [(x1, y1), (x2, y2)]
                                                                           [x1, y1] -> [(x1, y1)]
                        rectCoordsToLineCoords [(x1, y1), (x2, y2)] = [ (x,y) | x <- [x1, x2], y <- [y1, y2] ] ++
                                                                      [ (x,y) | y <- [y1, y2], x <- [x1, x2] ]

printCanvasThenRepl :: CanvasStack -> IO String
printCanvasThenRepl stack = do printCanvas $ fst $ runWriter stack
                               repl stack

printCanvas :: Canvas -> IO ()
printCanvas canvas = putStrLn $ concatMap ((" " ++) . charToString) $ unlines canvas
                      where charToString c = [c]

undo :: CanvasStack -> CanvasStack
undo stack = let oldStack = tail $ execWriter stack
             in writer (head oldStack, oldStack)

makeNewCanvas :: [Coord] -> Canvas
makeNewCanvas [(x, y)] = replicate x $ concat $ replicate y "o"

drawLineOnCanvas :: Canvas -> [Coord] -> Canvas
drawLineOnCanvas canvas [(x1, y1),(x2, y2)] = let canvasBeforeLine = take y1 canvas 
                                                  drawnOnBit = concatMap (\y -> drawOnLineBetween x1 x2 (canvas !! y)) [y1..y2] 
                                                  (_, canvasAfterLine) = splitAt (y2+1) canvas 
                                               in canvasBeforeLine ++ drawnOnBit ++ canvasAfterLine
                                        
drawOnLineBetween :: Int -> Int -> String -> Canvas
drawOnLineBetween x1 x2 line = let lineBeforeDraw = [take x1 line]
                                   drawnOnLine = [concat $ replicate (x2 - x1 + 1) "x"]
                                   (_, lineAfterDraw) = splitAt (x2 + 1) line 
                               in [concat (lineBeforeDraw ++ drawnOnLine ++ [lineAfterDraw]) ]

drawRectangle :: [Coord] -> Canvas -> Canvas
drawRectangle coords canvas = let ([(x1,y1), (x2,y2)], restOfCoords) = splitAt 2 coords
                              in case restOfCoords of [] -> drawLineOnCanvas canvas [(x1,y1), (x2,y2)]
                                                      _ -> drawRectangle restOfCoords $ drawLineOnCanvas canvas [(x1,y1), (x2,y2)]

bucketFill :: Canvas -> [Coord] -> Canvas
bucketFill canvas coords = let ([(x,y)], restOfCoords) = splitAt 1 coords
                               drawnOnCanvas = drawLineOnCanvas canvas [(x,y),(x,y)]
                               neighbours = getValidNeighbours canvas (x, y)
                               activeCoords = neighbours ++ restOfCoords
                           in case activeCoords of [] -> drawnOnCanvas
                                                   _ -> bucketFill drawnOnCanvas activeCoords

getValidNeighbours :: Canvas -> Coord -> [Coord]
getValidNeighbours canvas (x,y) =
    let coords = [ (x',y') | x' <- [x - 1 .. x + 1], y' <- [y - 1 .. y + 1]] 
        inCanvasCoords = filter validCoord coords
        canvasExcludingThisCoord = filter notSameCoord inCanvasCoords
    in  filter validColor canvasExcludingThisCoord
  where
    (canvasWidth, canvasHeight) = (length (head canvas), length canvas)
    validCoord (i, j) = i >= 0 && i < canvasWidth  && j >= 0 && j < canvasHeight
    validColor (i, j) = ((canvas !! j) !! i) == ((canvas !! y) !! x) && ((canvas !! j) !! i) /= 'x'
    notSameCoord (i, j) = (i, j) /= (x, y)
















